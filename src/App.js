import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'semantic-ui-css/semantic.min.css';
import SideBar from './Header/SideBar';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Image from 'react-bootstrap/Image';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import logo from './Images/cogal-icon.jpg';
import searchIcon from './Images/filter.png';
import mountain from './Images/Mountain.mp4';

class App extends Component {

  state = {

    isClicked: false,
    opacity: 0
  }

  searchBtnHandler = (event) => {
    console.log("clicked");
    const show = this.state.isClicked;
    this.setState({ isClicked: !show });
  }

  render() {

    const logoStyle = {
      width: '300px',
      height: 'auto'
    }

    const imgStyle = {
      width: '25px',
      height: '20px'
    }

    let bodyOpaque = "bodyNotOpaque";
    if (this.state.isClicked) {
      bodyOpaque = "bodyOpaque";
    }

    return (
      <div className="App">
        <header>
        </header>
        
          {/* <Header
          clicked={this.searchBtnHandler}
          isHidden={this.state.isHiddenNavBar} /> */}

          <Image src={logo} style={logoStyle} />
          <Button variant="light" onClick={this.searchBtnHandler}>
            <Image src={searchIcon} style={imgStyle} />
          </Button>
          <Navbar>
            <SideBar isClicked={this.state.isClicked} onclick={this.searchBtnHandler} />
          </Navbar>

          <div className={bodyOpaque}>
            <Jumbotron className='MainJumbotron'>
              <Container>
                <Row>
                  <Col>
                    <video className="vidJumbotron" muted autoPlay loop>
                      <source src={mountain} type="video/mp4">
                      </source>
                    </video>
                  </Col>
                </Row>
              </Container>
            </Jumbotron>
          </div>
          <div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sed euismod eros. Fusce lacinia convallis dolor, quis molestie dolor consequat eget. Integer sit amet vestibulum lectus. Donec sit amet sapien at purus vulputate bibendum quis eu mauris. Integer hendrerit metus neque, nec commodo nulla suscipit ac. Suspendisse vitae pulvinar justo. Sed consectetur volutpat risus congue tempus. Etiam pharetra sit amet enim id luctus. Praesent sed metus volutpat, aliquet lacus vel, sodales justo. Proin eu leo eget massa ultrices volutpat. In vel mauris gravida, sodales est vel, eleifend ex. Nullam id lectus dictum, vehicula lacus ac, placerat sem. Morbi pulvinar dui ligula, vel malesuada urna dignissim vel.

            Quisque maximus magna vel arcu vehicula, sit amet iaculis enim venenatis. Duis laoreet fringilla porttitor. Integer egestas interdum justo at vestibulum. In non purus sodales erat cursus rutrum. Nullam hendrerit lacus at nisl sodales pretium. Sed eu pharetra nisl. Nullam ut lorem venenatis, porttitor libero sit amet, finibus libero. Mauris efficitur dolor a ipsum volutpat vestibulum. Quisque in mauris et justo dapibus hendrerit ac quis massa. Nulla ac sem ipsum. Mauris in nunc massa. Nunc interdum vulputate lectus, convallis eleifend neque.

            Sed pretium ut mi sit amet fermentum. Fusce rutrum dui eget nisi suscipit, a tincidunt nunc pellentesque. Ut convallis diam nec metus tincidunt vulputate. Nam eu lacus nec metus pulvinar elementum. Sed feugiat maximus enim rhoncus posuere. Etiam volutpat odio vel tortor ornare porta. Sed varius ante non est tincidunt, scelerisque tincidunt lectus dictum. Aliquam erat volutpat. Sed auctor et velit ac cursus. In hac habitasse platea dictumst. Morbi nunc augue, dapibus ut nibh quis, laoreet tristique libero. Ut sed lacus nec tellus gravida pretium sed eu felis. Donec enim ante, faucibus non tempus a, congue eu nibh.

            Vivamus fringilla ut nisl eget finibus. Praesent at purus dictum, lacinia leo quis, ullamcorper nibh. Aliquam erat volutpat. Proin mollis ipsum vitae ex semper varius. Aenean efficitur, ante a mattis suscipit, ligula tortor tempus ante, nec tempus eros dui at erat. Phasellus euismod magna nec blandit dapibus. In ornare turpis vehicula elit tincidunt, nec gravida odio elementum.

          Suspendisse potenti. Aliquam condimentum ullamcorper risus sed viverra. Sed vitae dui tempus, gravida eros vitae, lobortis dui. Proin vehicula ac sapien ut luctus. Morbi scelerisque magna vel lorem luctus feugiat eu a tortor. Morbi pretium orci felis, ac luctus sem posuere et. Vestibulum pulvinar vel metus ac tristique. Phasellus a imperdiet risus, sed dapibus justo. Nullam maximus a lectus sed iaculis. Aliquam efficitur gravida lacus, non eleifend mi tempus a. Vivamus sollicitudin elit in efficitur bibendum. Pellentesque fringilla egestas elit sagittis feugiat. Cras libero metus, ullamcorper vel dolor vel, lacinia aliquet ipsum. Morbi dignissim ligula vel nisl sollicitudin gravida.</p>
          </div>
        </div>
    );
  };
}

export default App;