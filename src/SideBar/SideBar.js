import React, {useState } from 'react';
import Nav from 'react-bootstrap/Nav';
import NavBar from 'react-bootstrap/NavBar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
import './SideBar.css';
import {
    Checkbox,
    Grid,
    Header,
    Icon,
    Image,
    Menu,
    Segment,
    Sidebar,
  } from 'semantic-ui-react';

const SideBar = (props) => {

    const closeBtn = {
        position: 'absolute',
        top: '0',
        right: '25px',
        fontSize: '36px',
        marginLeft: '50px'
    }

    // let sideBar = null;
    // if (!props.isHidden) {
    //     sideBar = (
    //         <Nav defaultActiveKey="/home" className='flex-column SideNav' >
    //             <Nav.Link href="/home">&times;</Nav.Link>
    //             <Nav.Link href="/home">Home</Nav.Link>
    //             <Nav.Link eventKey="link-1">About Us</Nav.Link>
    //             <Nav.Link eventKey="link-2">Services</Nav.Link>
    //             <Nav.Link eventKey="disabled">Our Clients</Nav.Link>
    //             <Nav.Link eventKey="disabled">Blog</Nav.Link>
    //             <Nav.Link eventKey="disabled">Contact Us</Nav.Link>
    //         </Nav>)
    // }
    let sideNavClassName = 'flex-column SideNav closeSideNav';
    if( props.isClicked){
        sideNavClassName = 'flex-column SideNav openSideNav';
    }

    return(
        <div>
            <Nav defaultActiveKey="/home" className={sideNavClassName} >
                <Nav.Link style={closeBtn} onClick={props.onclick}>&times;</Nav.Link>
                <Nav.Link>Home</Nav.Link>
                <Nav.Link eventKey="link-1">About Us</Nav.Link>
                <Nav.Link eventKey="link-2">Services</Nav.Link>
                <Nav.Link eventKey="disabled">Our Clients</Nav.Link>
                <Nav.Link eventKey="disabled">Blog</Nav.Link>
                <Nav.Link eventKey="disabled">Contact Us</Nav.Link>
            </Nav>
        </div>
    );
    // const [visible, setVisible] = React.useState(false)
    // return (
    //     <Grid columns={1}>
    //       <Grid.Column>
    //         <Checkbox
    //           checked={visible}
    //           label={{ children: <code>visible</code> }}
    //           onChange={(e, data) => setVisible(data.checked)}
    //         />
    //       </Grid.Column>
    
    //       <Grid.Column>
    //         <Sidebar.Pushable as={Segment}>
    //           <Sidebar
    //             as={Menu}
    //             animation='overlay'
    //             icon='labeled'
    //             inverted
    //             onHide={() => setVisible(false)}
    //             vertical
    //             visible={visible}
    //             width='thin'
    //           >
    //             <Menu.Item as='a'>
    //               About Us
    //             </Menu.Item>
    //             <Menu.Item as='a'>
    //               Services
    //             </Menu.Item>
    //             <Menu.Item as='a'>
    //               Our Clients
    //             </Menu.Item>
    //             <Menu.Item as='a'>
    //               Blog
    //             </Menu.Item>
    //             <Menu.Item as='a'>
    //               Contact Us
    //             </Menu.Item>
    //           </Sidebar>
    
    //           <Sidebar.Pusher dimmed={visible}>
    //             <Segment basic>
    //               <Header as='h3'>Application Content</Header>
    //               <Image src='/images/wireframe/paragraph.png' />
    //             </Segment>
    //           </Sidebar.Pusher>
    //         </Sidebar.Pushable>
    //       </Grid.Column>
    //     </Grid>
    //   )
    
}

export default SideBar;